
CREATE TABLE transactions (
  int_reference int(11) NOT NULL AUTO_INCREMENT,
  dec_amount decimal(10,2) NOT NULL,
  int_account_nr int(11) NOT NULL,
  date_created datetime DEFAULT NULL,
  PRIMARY KEY (int_reference)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE balances (
  int_account_nr int(11) NOT NULL AUTO_INCREMENT,
  dec_balance decimal(10,2) DEFAULT NULL,
  date_created datetime DEFAULT NULL,
  PRIMARY KEY (int_account_nr)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- #TRANSACTION SAMPLE
-- START TRANSACTION;
-- SELECT @balance := dec_balance FROM balances WHERE int_account_nr = 1 FOR UPDATE;
-- SET @deduct = IF(@balance >= 10, -10, NULL);
-- SET @add = IF(@balance >= 10, +10, NULL);
-- UPDATE balances SET dec_balance = dec_balance+@deduct WHERE int_account_nr = 1;
-- UPDATE balances SET dec_balance = dec_balance+@add WHERE int_account_nr = 2;
-- INSERT INTO transactions (dec_amount, int_account_nr) VALUES (@deduct,1),(@add,2);
-- COMMIT;
