# nodetest-1

Project Setup

1. npm install
2. Create a database and execute DB/table.sql file
3. make a copy of env.example and modify your db credentials
To start the server
1. `node index.js`
2. To make changes use nodemon(if `nodemon` is installed globally else use `npx nodemon`)

Routes
##Get Info
GET /
##Get all account balances
GET /balances
##Get account balance by account number
GET /balances/:accountNumber
##Create account
POST /balances Body:{"balance":2500.50}
##Transfer balannce from source account(from) to target account(to)
POST /transfer Body:{"from":1, "to":2, "amount":10}


Transaction Details
Query used:
```
source account: 1
target account: 2
transaction amount: 10
```

```
START TRANSACTION;
```
#####Lock the table for update so that any further queries will wait for the transaction to be completed
```
SELECT @balance := dec_balance FROM balances WHERE int_account_nr = 1 FOR UPDATE;
```
#####Set balance to be deducted/added as NULL if balance of source account(1) is less than the transaction amount, 
```
SET @deduct = IF(@balance >= 10, -10, NULL);
SET @add = IF(@balance >= 10, +10, NULL);
```
#####Deduct the transaction amount from source account(1)
```
UPDATE balances SET dec_balance = dec_balance+@deduct WHERE int_account_nr = 1;
```
#####Add the transaction amount to the target account
```
UPDATE balances SET dec_balance = dec_balance+@add WHERE int_account_nr = 2;
```
#####Insert deucted transaction amount from the source account(1) as a transaction in transaction table
#####Insert added transaction amount to the target account(2) as a transaction in transaction table
#####When inserting the transaction to table, if the deduct/add is NULL, will throw error and fail which will rollback the transaction as NULL is not allowed in transaction table transaction amount field
```
INSERT INTO transactions (dec_amount, int_account_nr) VALUES (@deduct,1),(@add,2);
```
#####If everything is fine, COMMIT
```
COMMIT;
```