var express = require('express'),
    app = express(),
    port = 8081;

let Database = require('./utils/db.js');
let database = new Database();

let Transactions = require('./utils/transactions.js');

const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.listen(port, function () {
    console.log('listening on *:' + port);
});
app.post("/transfer", async function (req, res) {
    try {
        let transactions = new Transactions();
        var { body } = (req.body);
        let amount = parseFloat(req.body.amount);
        let from = parseInt(req.body.from);
        let to = parseInt(req.body.to);
        let error = null;

        if (isNaN(amount) || amount <= 0) {
            error = 'Invalid amount';
        }
        else if (isNaN(from) || from < 0) {
            error = 'Invalid from';
        }
        else if (isNaN(to) || to < 0) {
            error = 'Invalid to';
        }
        else if(from == to)
        {
            error = 'From and To account are same';
        }
        else {
            //Check from account exists and has enough balance
            let account1 = await transactions.getBalances(from);
            if (!account1) {
                error = 'From account not found';
            }
            else if (account1[0].balance != 0 && account1[0].balance < amount) {
                error = 'Insufficient balance';
            }

            //Check to account exists
            let account2 = await transactions.getBalances(to);
            if (!account2) {
                error = 'To account not found';
            }
        }

        if (error != null) {
            return res.status(400).send({ "message": error, "request": req.body });
        }
        else {
            let transferStatus = await transactions.transferMoney(from, to, amount).catch((err) => {
                return res.status(400).send({ message: "Transaction failed", request: req.body });
            });
            let fromAccountInfo = await transactions.getBalances(from).catch((err) => {
                console.log(err);
            });
            let toAccountInfo = await transactions.getBalances(to).catch((err) => {
                console.log(err);
            });
            res.send({
                status: transferStatus,
                from: fromAccountInfo,
                to: toAccountInfo
            });
        }

    } catch (error) {
        res.status(500).send({ message: "Transaction failed", request: req.body });
    }
});

app.post("/balances/", async function (req, res) {
    try {
        let transactions = new Transactions();
        var balance = parseFloat(req.body.balance);
        if (isNaN(balance) || balance < 0) {
            res.status(400).send({ "message": "Invalid balance: ", "request": req.body });
        }
        else {
            await transactions.createAccount(balance).then(function (results) {
                res.send(results);
            })
        }
    }
    catch (err) {
        res.status(500).send({ message: "Failed to retrieve balance" });
    }
});


app.get("/balances/", async function (req, res) {
    try {
        let transactions = new Transactions();
        await transactions.getBalances().then(function (results) {
            if (results) {
                res.send({ results: results });
            }
            else {
                res.status(404).send({ "message": "No records found" });
            }
        })
    }
    catch (err) {
        res.status(500).send({ message: "Failed to retrieve balance" });
    }
});


app.get("/balances/:account", async function (req, res) {
    try {
        let transactions = new Transactions();
        var { account } = req.params;
        let results = await transactions.getBalances(account);
        if (results && results[0]) {
            res.send(results[0]);
        }
        else {
            res.status(404).send({ "message": "No records found" });
        }
    }
    catch (error) {
        res.status(500).send({ message: "Failed to retrieve balance" });
    }
});



app.use("/", async function(req, res){
    res.send({project:'Namshi assesment', author:'Anas<anas.muhammed@gmail.com>'})
})