const mysql = require('mysql');
require('dotenv').load();

class Database {
    constructor() {
        this.getPool();
    }
    async createConnection() {
        let me = this;
        return new Promise((resolve, reject) => {
            try {
                if (this.connection) {
                    //If connection exists, return this.connection
                    resolve(this.connection);
                }
                else {
                    if (!this.pool) {
                        //If no pool exists, create pool
                        this.getPool();
                    }
                    //Get connection from pool
                    this.pool.getConnection(function (err, connection) {
                        if (err) return reject(err); // not connected!
                        me.connection = connection;
                        resolve(me.connection);
                    })
                }
            } catch (err) {
                reject(err);
            }
        })
    }
    getPool(cb = () => { }) {
        //If no pool exists, create pool
        if (this.pool) return this.pool;
        try {
            this.pool = mysql.createPool({
                host: process.env.DB_CONN_HOST,
                user: process.env.DB_CONN_USER,
                password: process.env.DB_CONN_PWD,
                database: process.env.DB_CONN_DB,
            });
            return cb(null, this.pool)
        } catch (error) {
            return cb(error);
        }
    }
    async getConnection() {
        return await this.createConnection();
    }
    async query(sql, args) {
        if (!this.connection) {
            this.connection = await this.getConnection();
        }
        return new Promise((resolve, reject) => {
            let query = this.connection.query(sql, args, (err, rows) => {
                if (err) {
                    return reject(err);
                }
                //To see the executed queries
                //console.log(query.sql);
                resolve(rows);
            });
        })
    }
    close() {
        return new Promise((resolve, reject) => {
            if (this.connection) {
                this.connection.end(err => {
                    if (err)
                        return reject(err);
                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
    }
}

module.exports = Database
