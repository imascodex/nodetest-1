var Database = require('./db.js');


class Transaction {
    constructor(config) {
        //this.getPool();
    }
    async createAccount(balance) {
        return new Promise((resolve, reject) => {
            try {
                let database = new Database();
                let query = "INSERT INTO balances(`int_account_nr`, `dec_balance`, `date_created`) VALUES (?, ?, ?)";
                let params = [null, balance, new Date()];
                database.query(query, params).then(results => {
                    if (results.insertId) {
                        let accountNumber =results.insertId;
                        let query = "INSERT INTO transactions(`dec_amount`, `int_account_nr`, `date_created`) VALUES (?, ?, ?)";
                        let params = [balance, accountNumber, new Date()];
                        database.query(query, params).then(results => {
                            if (results.insertId) {
                                resolve({ "message": "Record created", "account_number": accountNumber });
                            }
                        })
                    }
                    else {
                        reject({ "message": "Could not create account" });
                    }
                })
            }
            catch (err) {
                reject(err);
            }
        })
    }
    async getBalances(account_number) {
        return new Promise((resolve, reject) => {
            try {
                let database = new Database();
                let query = 'SELECT * FROM balances';
                let params = [];

                if (typeof account_number != 'undefined') {
                    query += ' WHERE int_account_nr=?';
                    params.push(account_number);
                }

                database.query(query, params).then(results => {
                    if (results.length > 0) {
                        let records = [];
                        results.forEach(function (rows) {
                            records.push({ account_number: rows.int_account_nr, balance: rows.dec_balance, created_date: rows.date_created })
                        })
                        resolve(records);
                    }
                    else {
                        resolve(null);
                    }
                }).catch((err) => {
                    reject(err);
                });
            }
            catch (err) {
                reject(err);
            }
        })
    }
    async transferMoney(from, to, amount) {
        let database = new Database();
        let connection = await database.getConnection();
        return new Promise((resolve, reject) => {
            try {
                connection.ping(function (err) {
                    if (err) reject(err);
                })
                connection.beginTransaction(function (err) {
                    if (err) { reject(err); }

                    connection.query('SELECT @balance := dec_balance FROM balances WHERE int_account_nr = ? FOR UPDATE', [from], function (error, results, fields) {
                        if (error) {
                            return connection.rollback(function () {
                                console.log(`\n--------------\nError - 1\n--------------\n`);
                                reject(error);
                            });
                        }
                        console.log(`\n--------------\nNo error - 1\n--------------\n`);
                        console.log(results);

                        //Set balance to be deducted as NULL if balance is less than amount, which will fail the transactio as NULL is not allowd in transaction table
                        connection.query('SET @deduct = IF(@balance >= ?, -?, NULL)', [amount, amount], function (error, results, fields) {
                            if (error) {
                                return connection.rollback(function () {
                                    console.log(`\n--------------\nError - 2\n--------------\n`);
                                    reject(error);
                                });
                            }
                            console.log(`\n--------------\nNo error - 2\n--------------\n`);
                            console.log(results);


                            connection.query('SET @add = IF(@balance >= ?, ?, NULL)', [amount, amount], function (error, results, fields) {
                                if (error) {
                                    return connection.rollback(function () {
                                        console.log(`\n--------------\nError - 3\n--------------\n`);
                                        reject(error);
                                    });
                                }

                                console.log(`\n--------------\nNo error - 3\n--------------\n`);
                                console.log(results);

                                connection.query('INSERT INTO transactions (dec_amount, int_account_nr,date_created) VALUES (@deduct, ?, ?), (@add, ?, ?)', [from, new Date(), to, new Date()], function (error, results, fields) {
                                    if (error) {
                                        return connection.rollback(function () {
                                            console.log(`\n--------------\nError - 4\n--------------\n`);
                                            reject(error);
                                        });
                                    }

                                    console.log(`\n--------------\nNo error - 4\n--------------\n`);
                                    console.log(results);

                                    connection.query('UPDATE balances SET dec_balance =dec_balance+@add WHERE int_account_nr=?', [to], function (error, results, fields) {
                                        if (error) {
                                            return connection.rollback(function () {
                                                console.log(`\n--------------\nError - 5\n--------------\n`);
                                                reject(error);
                                            });
                                        }

                                        console.log(`\n--------------\nNo error - 5\n--------------\n`);
                                        console.log(results);

                                        connection.query('UPDATE balances SET dec_balance =dec_balance+@deduct WHERE int_account_nr=?', [from], function (error, results, fields) {
                                            if (error) {
                                                return connection.rollback(function () {
                                                    console.log(`\n--------------\nError - 6\n--------------\n`);
                                                    reject(error);
                                                });
                                            }

                                            console.log(`\n--------------\nNo error - 6\n--------------\n`);
                                            console.log(results);

                                            connection.commit(function (err) {
                                                if (err) {
                                                    connection.rollback(function () {
                                                        console.log(`\n--------------\nError - 7\n--------------\n`);
                                                        reject(error);
                                                    });
                                                }
                                                console.log(`\n--------------\nNo error - 6\n--------------\n`);
                                                console.log(results);
                                                console.log('Transaction Complete.');

                                                resolve('SUCCESS');
                                            });
                                        })
                                    })
                                })
                            })
                        })
                    })
                });
            }
            catch (err) {
                reject(err);
            }
        })
    }
}

module.exports = Transaction;
